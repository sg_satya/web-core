﻿
namespace SGeeks.Core
{
    public static class SGeeksVersion
    {
        /// <summary>
        /// Gets or sets the store version
        /// </summary>
        public static string CurrentVersion 
        {
            get
            {
                return "4.00";
            }
        }
    }
}
